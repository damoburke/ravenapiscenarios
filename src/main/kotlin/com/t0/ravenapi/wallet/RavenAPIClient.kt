package com.t0.ravenapi.wallet

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import feign.Logger
import feign.Response
import feign.Util
import feign.codec.ErrorDecoder
import io.medici.kresult.dto.Message
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestHeader
import java.math.BigDecimal
import java.nio.charset.StandardCharsets
import java.util.*
import kotlin.collections.ArrayList

@FeignClient(
    name = "ravenapi",
    url = "https://api.raven-testnet.tzero.com",
    configuration = [RavenAPIConfig::class]
)
interface RavenAPIClient {

    @JvmDefault
    fun tryCreateToken(
        request: UserAPIRequest
    ) = tryCall {
        createToken(request)
    }

    @PostMapping(
        "api/create", headers = [HOST, USER_AGENT, "Connection: keep-alive", "Accept: */*"]
    )
    fun createToken(request: UserAPIRequest): ResponseEntity<UserAPIResponse>


    @PostMapping("recovery/start", headers = [HOST, USER_AGENT, "Connection: keep-alive", "Accept: */*"])
    fun startRecovery(
        @RequestHeader(HttpHeaders.AUTHORIZATION) authHeader: String?,
        request: AddressRequest
    ): ResponseEntity<AddressResponse>

    @JvmDefault
    fun tryStartRecovery(authHeader: String, request: AddressRequest) = tryCall {
        startRecovery(authHeader, request)
    }

    @PostMapping("recovery/status", headers = [HOST, USER_AGENT, "Connection: keep-alive", "Accept: */*"])
    fun recoveryStatus(
        @RequestHeader(HttpHeaders.AUTHORIZATION) authHeader: String?,
        request: StatusRequest
    ): ResponseEntity<AddressResponseFinal>

    @JvmDefault
    fun tryRecoveryStatus(authHeader: String, request: StatusRequest) = tryCall {
        recoveryStatus(authHeader, request)
    }


    @PostMapping("rpc/createTransaction", headers = [HOST, USER_AGENT, "Connection: keep-alive", "Accept: */*"])
    fun createTransaction(
        @RequestHeader(HttpHeaders.AUTHORIZATION) authHeader: String?,
        request: CreateTransactionRequest
    ): ResponseEntity<CreateTransactionResponse>

    @JvmDefault
    fun tryCreateTransaction(authHeader: String, request: CreateTransactionRequest) = tryCall {
        createTransaction(authHeader, request)
    }

    @PostMapping(
        "rpc/sendTransaction",
        headers = [HOST, USER_AGENT, "Connection: keep-alive", "Accept: */*"]
    )
    fun sendTransaction(
        @RequestHeader(HttpHeaders.AUTHORIZATION) authHeader: String?,
        request: SendTransactionRequest
    ): ResponseEntity<SendTransactionResponse>

    @JvmDefault
    fun trySendTransaction(authHeader: String, request: SendTransactionRequest) = tryCall {
        sendTransaction(authHeader, request)
    }

    @PostMapping(
        "rpc/signTransaction-DevOnlyEndpoint",
        headers = [HOST, USER_AGENT, "Connection: keep-alive", "Accept: */*"]
    )
    fun signTransaction(
        @RequestHeader(HttpHeaders.AUTHORIZATION) authHeader: String?,
        request: SignTransactionRequest
    ): ResponseEntity<SignTransactionResponse>

    @JvmDefault
    fun trySignTransaction(authHeader: String, request: SignTransactionRequest) = tryCall {
        signTransaction(authHeader, request)
    }

    @PostMapping(
        "rpc/transactionDetailsForAddresses",
        headers = [HOST, USER_AGENT, "Connection: keep-alive", "Accept: */*"]
    )
    fun getTransactionDetailsForAddresses(
        @RequestHeader(HttpHeaders.AUTHORIZATION) authHeader: String?,
        request: TransactionDetailsForAddressesRequest
    ): ResponseEntity<TransactionDetailsForAddressesResponse>

    @JvmDefault
    fun tryGetTransactionDetailsForAddresses(authHeader: String, request: TransactionDetailsForAddressesRequest) =
        tryCall {
            getTransactionDetailsForAddresses(authHeader, request)
        }

    @PostMapping("rpc/blockCountDamo", headers = [HOST, USER_AGENT, "Connection: keep-alive", "Accept: */*"])
    fun getBlockCountDamo(
        @RequestHeader(HttpHeaders.AUTHORIZATION) authHeader: String?,
        request: String? = ""
    ): ResponseEntity<BlockCountResponse>

    @JvmDefault
    fun tryGetBlockCountDamo(authHeader: String) = tryCall {
        getBlockCountDamo(authHeader)
    }

}

@Configuration
class RavenAPIConfig {
    @Bean
    fun feignLoggerLevel(): Logger.Level {
        return Logger.Level.FULL
    }

    class Config {
        @Bean
        fun errorDecoder(objectMapper: ObjectMapper) = TZeroErrorDecoder(objectMapper)
    }
}

const val HOST = "Host=damo.com"
const val USER_AGENT = "User-Agent=damo-test"

data class TZeroResponse<T>(val response: ResponseEntity<T>? = null, val e: TZeroException? = null)

data class TZeroException(
    val response: Response,
    override val message: String,
    val responseMessage: Message? = null
) : RuntimeException(message)

fun <T> tryCall(call: () -> ResponseEntity<T>): TZeroResponse<T> {
    return try {
        TZeroResponse(response = call())
    } catch (e: TZeroException) {
        TZeroResponse(e = e)
    }
}

class TZeroErrorDecoder(private val objectMapper: ObjectMapper) : ErrorDecoder {

    override fun decode(methodKey: String, response: Response): Exception {
        val body = getBodyAsString(response)
        val errorResponse = tryDecodeErrorResponse(body)
        return TZeroException(response, "Error calling service", errorResponse)
    }

    private fun getBodyAsString(response: Response): String? {
        return if (response.body() != null) {
            Util.toString(response.body().asReader(StandardCharsets.UTF_8))
        } else {
            null
        }
    }

    private fun tryDecodeErrorResponse(response: String?): Message? {
        return try {
            objectMapper.readValue(response, Message::class.java)
        } catch (e: Exception) {
            null
        }
    }
}

data class UserAPIRequest(val email: String, val uuid: UUID, val p: String)

data class UserAPIResponse(val token: String, val refreshToken: String)

data class CreateTransactionRequest(
    val fromAddresses: List<String>,
    val recipient: String,
    val sendMax: Boolean? = false,
    val changeAddress: String? = null,
    val amount: BigDecimal?,
    val deductFee: Boolean
)

data class SignTransactionRequest(
    val transaction: String,
    val keys: Array<String>
)

data class SignTransactionResponse(
    val complete: Boolean,
    val hex: String
)

data class SendTransactionRequest(
    val transaction: String
)

data class SendTransactionResponse(
    val transactionId: String
)

data class TransactionDetailsForAddressesRequest(
    val addresses: List<String>,
    val transactionIds: List<String>
)

class TransactionDetailsForAddressesResponse : ArrayList<SSItem>()

data class SSItem(
    val block: Int,
    val category: String,
    val confirmations: Int,
    val details: Details,
    val networkFee: Double,
    val timestamp: Int,
    val transactionId: String,
    val value: Double
)

data class Details(
    val deltas: List<Delta>,
    val inputs: List<Input>,
    val outputs: List<Output>
)

data class Input(
    val address: String,
    val value: Int
)

data class Output(
    val address: String,
    val value: Int
)

data class Delta(
    val address: String,
    val value: Double
)

data class CreateTransactionResponse(
    val addresses: List<String>,
    val fee: Double,
    val total: Double,
    val unsignedHex: String
)

data class BlockCountResponse(
    val blockCount: Int
)

data class AddressRequest(
    @JsonProperty("extended_public_key")
    val extendedPublicKey: String
)

data class AddressResponse(
    val recoveryId: Int
)

data class StatusRequest(
    @JsonProperty("recovery_id")
    val recoveryId: Int
)

data class AddressResponseFinal(
    val elapsed: Double,
    val result: Result?,
    val status: String
)

data class Result(
    val changeAddresses: List<String>,
    val changeIndex: Int,
    val receiveAddresses: List<String>,
    val receiveIndex: Int
)