package com.t0.ravenapi.wallet

import com.t0.ravenapi.logger
import io.medici.kresult.MediciError
import io.medici.kresult.Result
import org.bitcoinj.core.ECKey
import org.bitcoinj.core.LegacyAddress
import org.bitcoinj.core.Transaction
import org.bitcoinj.core.Utils
import org.bitcoinj.core.Utils.HEX
import org.bitcoinj.crypto.ChildNumber
import org.bitcoinj.crypto.DeterministicHierarchy
import org.bitcoinj.crypto.DeterministicKey
import org.bitcoinj.crypto.HDKeyDerivation
import org.bitcoinj.crypto.HDUtils
import org.bitcoinj.crypto.TransactionSignature
import org.bitcoinj.script.ScriptBuilder
import org.bitcoinj.script.ScriptPattern
import org.bitcoinj.wallet.DeterministicSeed
import org.springframework.util.Base64Utils
import ravencoin.params.RvnTestNetParams
import javax.inject.Named

val params = RvnTestNetParams()

interface WalletService {

    fun sign(serializedTx: String): Result<MediciError, String>

    /**
     * https://github.com/RavenDevKit/ravencore-lib/blob/master/docs/hierarchical.md
     */
    fun restore(tZeroUserId: Int, index: Int): Wallet

}

@Named
class WalletServiceImpl : WalletService {

    val log = logger()

    lateinit var accountKey: DeterministicKey

    init {
        var mnemonic =
            "gasp anxiety poverty satisfy certain fitness visual song poverty bicycle early file ethics awesome split"
        val passphrase = ""
        val deterministicSeed = DeterministicSeed(mnemonic, null, passphrase, 0)
        val rootPrivateKey = HDKeyDerivation.createMasterPrivateKey(deterministicSeed.seedBytes)

        log.info("**********use root private key to generate sub private/public key and address****************")
        val deterministicHierarchy = DeterministicHierarchy(rootPrivateKey)
        val path = HDUtils.parsePath("44H/1H/0H")
        log.info("is isHardened: ${path[0].isHardened}")

        accountKey = deterministicHierarchy.get(path, true, true)
        log.info("Account 0 Path: ${accountKey.pathAsString}")
        log.info("Account 0 Extended Public Key: ${accountKey.serializePubB58(params)}")
        if (!accountKey.isPubKeyOnly) {
            log.info("Account 0 Extended Private Key: ${accountKey.serializePrivB58(params)} ")
        }
    }

    override fun sign(serializedTx: String): Result<MediciError, String> {
        val wallet = restore(tZeroUserId = 1001, index = 10)
        val signedTransaction = signTransaction(wallet.key, serializedTx)
        return Result.value(HEX.encode(signedTransaction.signedTx))
    }

    override fun restore(tZeroUserId: Int, index: Int): Wallet {
        val key = HDKeyDerivation.deriveChildKey(accountKey, tZeroUserId)
        log.info("BIP32 Derivation Path: ${key.pathAsString}")
        log.info("BIP32 Extended Public Key: ${key.serializePubB58(params)}")
        log.info("BIP32 Extended Private Key: ${key.serializePrivB58(params)}")

        return derivedAddresses(key, index)
    }

    private fun derivedAddresses(key: DeterministicKey, index: Int): Wallet {

        val subKey = HDKeyDerivation.deriveChildKey(key, ChildNumber(index, false))
        log.info("$index    Path        ${subKey.pathAsString}")
        log.info("$index    Private Key ${subKey.getPrivateKeyAsWiF(params)}")
        log.info("$index    Public      ${subKey.publicKeyAsHex}")

        return Wallet(
            key = subKey,
            address = LegacyAddress.fromPubKeyHash(params, subKey.pubKeyHash).toBase58(),
        )
    }

    fun signTransaction(ecKey: ECKey, transactionHex: String): SignedTransaction {
        val transaction = decodeTransactionHex(transactionHex)
        transaction.inputs.indices.forEach { i ->
            val input = transaction.getInput(i.toLong())
            val scriptPubKey = ScriptBuilder.createOutputScript(LegacyAddress.fromPubKeyHash(params, ecKey.pubKeyHash))
            val hash = transaction.hashForSignature(i, scriptPubKey, Transaction.SigHash.ALL, false)
            val ecSig = ecKey.sign(hash)
            val txSig = TransactionSignature(ecSig, Transaction.SigHash.ALL, false)
            if (ScriptPattern.isP2PK(scriptPubKey)) {
                input.scriptSig = ScriptBuilder.createInputScript(txSig, ecKey)
            } else {
                if (!ScriptPattern.isP2PKH(scriptPubKey)) {
                    return SignedTransaction(transaction, ByteArray(0))
                }
                input.scriptSig = ScriptBuilder.createInputScript(txSig, ecKey)
            }
        }
        return SignedTransaction(transaction, transaction.bitcoinSerialize())
    }

    fun decodeTransactionHex(transactionHex: String): Transaction {
        return Transaction(params, HEX.decode(transactionHex))
    }
}

data class SignedTransaction(val transaction: Transaction, val signedTx: ByteArray)

data class Wallet(val key: DeterministicKey, val address: String)