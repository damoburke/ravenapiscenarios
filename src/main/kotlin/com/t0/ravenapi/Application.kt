package com.t0.ravenapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@EnableFeignClients
@SpringBootApplication
class Runner

fun main(args: Array<String>) {
    runApplication<Runner>(*args)
}

