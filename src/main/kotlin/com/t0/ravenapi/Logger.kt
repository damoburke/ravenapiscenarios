package com.t0.ravenapi

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

fun <T : Any> logger(forClass: Class<T>): Logger {
    return LogManager.getLogger(unwrapCompanionClass(forClass).name)
}

fun <T : Any> unwrapCompanionClass(ofClass: Class<T>): Class<*> {
    return if (ofClass.enclosingClass != null &&
        ofClass.enclosingClass.kotlin.objectInstance?.javaClass == ofClass
    ) {
        ofClass.enclosingClass
    } else {
        ofClass
    }
}

fun <T : Any> T.logger(): Logger {
    return logger(this.javaClass)
}
