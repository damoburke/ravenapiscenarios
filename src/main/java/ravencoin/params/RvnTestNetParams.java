/*
 * Copyright 2013 Google Inc.
 * Copyright 2014 Andreas Schildbach
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ravencoin.params;

import com.google.common.base.Objects;
import org.bitcoinj.core.*;
import org.bitcoinj.net.discovery.HttpDiscovery;
import org.bitcoinj.params.AbstractBitcoinNetParams;
import org.bitcoinj.script.Script;
import org.bitcoinj.store.BlockStore;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.utils.MonetaryFormat;
import org.bitcoinj.utils.VersionTally;

import java.math.BigInteger;
import java.util.EnumSet;



public class RvnTestNetParams extends AbstractBitcoinNetParams {

    //   public static final long BLOCK_VERSION_GENESIS = 2;

    //   public static final int MAJORITY_WINDOW = 2016;

    public static final int ASSET_MESSAGING_VERSION = 70022;


    public RvnTestNetParams() {
        super();
        id = "org.ravencoin.testnet";
        addressHeader = 0x6f;
        p2shHeader = 0x7a;
        bip32HeaderP2PKHpub = 0x043587cf; // The 4 byte header that serializes in base58 to "tpub".
        bip32HeaderP2PKHpriv = 0x04358394; // The 4 byte header that serializes in base58 to "tprv"
        bip32HeaderP2WPKHpub = 0x045f1cf6; // The 4 byte header that serializes in base58 to "vpub".
        bip32HeaderP2WPKHpriv = 0x045f18bc; // The 4 byte header that serializes in base58 to "vprv"
//        dumpedPrivateKeyHeader = 239;
//        segwitAddressHrp = "1";

//        genesisBlock.setTime(1537466400L);
//        genesisBlock.setDifficultyTarget(0x1e00ffffL);
//        genesisBlock.setNonce(15615880);
//        final String genesisHash = genesisBlock.getHashAsString();
//        checkState(genesisHash.equals("000000ecfc5e6324a079542221d00e10362bdc894d56500c414060eea8a3ad5a"));

        //       port = 18770;
        //      maxTarget = Utils.decodeCompactBits(0x1e0fffffL);
        //       majorityWindow = MAJORITY_WINDOW;
        //       packetMagic = 0x52564E54; //R V N T
    }

//    @Override
//    public long getGenesisBlockVersion() {
//        return BLOCK_VERSION_GENESIS;
//    }

    /**
     * A Java package style string acting as unique ID for these parameters
     */
//    public String getId() {
//        throw new UnsupportedOperationException();
//    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return getId().equals(((NetworkParameters) o).getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    public int getSpendableCoinbaseDepth() {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns true if the block height is either not a checkpoint, or is a checkpoint and the hash matches.
     */
    public boolean passesCheckpoint(int height, Sha256Hash hash) {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns true if the given height has a recorded checkpoint.
     */
    public boolean isCheckpoint(int height) {
        throw new UnsupportedOperationException();
    }

    public int getSubsidyDecreaseBlockCount() {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns DNS names that when resolved, give IP addresses of active peers.
     */
    public String[] getDnsSeeds() {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns IP address of active peers.
     */
    public int[] getAddrSeeds() {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns discovery objects for seeds implementing the Cartographer protocol. See {@link HttpDiscovery} for more info.
     */
    public HttpDiscovery.Details[] getHttpSeeds() {
        throw new UnsupportedOperationException();
    }

    /**
     * <p>Genesis block for this chain.</p>
     *
     * <p>The first block in every chain is a well known constant shared between all Bitcoin implementations. For a
     * block to be valid, it must be eventually possible to work backwards to the genesis block by following the
     * prevBlockHash pointers in the block headers.</p>
     *
     * <p>The genesis blocks for both test and main networks contain the timestamp of when they were created,
     * and a message in the coinbase transaction. It says, <i>"The Times 03/Jan/2009 Chancellor on brink of second
     * bailout for banks"</i>.</p>
     */
    public Block getGenesisBlock() {
        throw new UnsupportedOperationException();
    }

    /** Default TCP port on which to connect to nodes. */
    public int getPort() {
        throw new UnsupportedOperationException();
    }

    /** The header bytes that identify the start of a packet on this network. */
    public long getPacketMagic() {
        throw new UnsupportedOperationException();
    }

//    /**
//     * First byte of a base58 encoded address. See {@link LegacyAddress}. This is the same as acceptableAddressCodes[0] and
//     * is the one used for "normal" addresses. Other types of address may be encountered with version codes found in
//     * the acceptableAddressCodes array.
//     */
//    public int getAddressHeader() {
//        throw new UnsupportedOperationException();
//    }

//    /**
//     * First byte of a base58 encoded P2SH address.  P2SH addresses are defined as part of BIP0013.
//     */
//    public int getP2SHHeader() {
//        throw new UnsupportedOperationException();
//    }

    /**
     * Comment out
     */

//    /**
//     * First byte of a base58 encoded dumped private key. See {@link DumpedPrivateKey}.
//     */
//    public int getDumpedPrivateKeyHeader() {
//        throw new UnsupportedOperationException();
//    }

//    /**
//     * Human readable part of bech32 encoded segwit address.
//     */
//    public String getSegwitAddressHrp() {
//        throw new UnsupportedOperationException();
//    }

    /**
     * How much time in seconds is supposed to pass between "interval" blocks. If the actual elapsed time is
     * significantly different from this value, the network difficulty formula will produce a different value. Both
     * test and main Bitcoin networks use 2 weeks (1209600 seconds).
     */
    public int getTargetTimespan() {
        throw new UnsupportedOperationException();
    }

    /**
     * If we are running in testnet-in-a-box mode, we allow connections to nodes with 0 non-genesis blocks.
     */
    public boolean allowEmptyPeerChain() {
        throw new UnsupportedOperationException();
    }

    /**
     * How many blocks pass between difficulty adjustment periods. Bitcoin standardises this to be 2016.
     */
    public int getInterval() {
        throw new UnsupportedOperationException();
    }

    /**
     * Maximum target represents the easiest allowable proof of work.
     */
    public BigInteger getMaxTarget() {
        throw new UnsupportedOperationException();
    }

    /**
     * The key used to sign {@link AlertMessage}s. You can use {@link ECKey#verify(byte[], byte[], byte[])} to verify
     * signatures using it.
     */
    public byte[] getAlertSigningKey() {
        throw new UnsupportedOperationException();
    }

    /** Returns the 4 byte header for BIP32 wallet P2PKH - public key part. */
//    public int getBip32HeaderP2PKHpub() {
//        throw new UnsupportedOperationException();
//    }

    /** Returns the 4 byte header for BIP32 wallet P2PKH - private key part. */
//    public int getBip32HeaderP2PKHpriv() {
//        throw new UnsupportedOperationException();
//    }

    /** Returns the 4 byte header for BIP32 wallet P2WPKH - public key part. */
//    public int getBip32HeaderP2WPKHpub() {
//        throw new UnsupportedOperationException();
//    }

    /** Returns the 4 byte header for BIP32 wallet P2WPKH - private key part. */
//    public int getBip32HeaderP2WPKHpriv() {
//        throw new UnsupportedOperationException();
//    }

    /**
     * The number of blocks in the last {@link #getMajorityWindow()} blocks
     * at which to trigger a notice to the user to upgrade their client, where
     * the client does not understand those blocks.
     */
    public int getMajorityEnforceBlockUpgrade() {
        throw new UnsupportedOperationException();
    }

    /**
     * The number of blocks in the last {@link #getMajorityWindow()} blocks
     * at which to enforce the requirement that all new blocks are of the
     * newer type (i.e. outdated blocks are rejected).
     */
    public int getMajorityRejectBlockOutdated() {
        throw new UnsupportedOperationException();
    }

    /**
     * The sampling window from which the version numbers of blocks are taken
     * in order to determine if a new block version is now the majority.
     */
    public int getMajorityWindow() {
        throw new UnsupportedOperationException();
    }

    /**
     * The flags indicating which block validation tests should be applied to
     * the given block. Enables support for alternative blockchains which enable
     * tests based on different criteria.
     *
     * @param block  block to determine flags for.
     * @param height height of the block, if known, null otherwise. Returned
     *               tests should be a safe subset if block height is unknown.
     */
    public EnumSet<Block.VerifyFlag> getBlockVerificationFlags(final Block block,
                                                               final VersionTally tally, final Integer height) {
        throw new UnsupportedOperationException();
    }

    /**
     * The flags indicating which script validation tests should be applied to
     * the given transaction. Enables support for alternative blockchains which enable
     * tests based on different criteria.
     *
     * @param block       block the transaction belongs to.
     * @param transaction to determine flags for.
     * @param height      height of the block, if known, null otherwise. Returned
     *                    tests should be a safe subset if block height is unknown.
     */
    public EnumSet<Script.VerifyFlag> getTransactionVerificationFlags(final Block block,
                                                                      final Transaction transaction, final VersionTally tally, final Integer height) {
        throw new UnsupportedOperationException();
    }

//    @Override
//    public Coin getMaxMoney() {
//        throw new UnsupportedOperationException();
//    }

    @Override
    public Coin getMinNonDustOutput() {
        throw new UnsupportedOperationException();
    }

    @Override
    public MonetaryFormat getMonetaryFormat() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getProtocolVersionNum(final ProtocolVersion version) {
//        return ProtocolVersion.ASSET_MESSAGING.getBitcoinProtocolVersion();
        //       throw new UnsupportedOperationException();
        return ASSET_MESSAGING_VERSION;
    }

//    @Override
//    public BitcoinSerializer getSerializer(boolean parseRetain) {
//        throw new UnsupportedOperationException();
//    }

    @Override
    public String getUriScheme() {
        throw new UnsupportedOperationException();
    }

//    @Override
//    public boolean hasMaxMoney() {
//        throw new UnsupportedOperationException();
//    }

    private static RvnTestNetParams instance;

    public static synchronized RvnTestNetParams get() {
        if (instance == null) {
            instance = new RvnTestNetParams();
        }
        return instance;
    }

    @Override
    public String getPaymentProtocolId() {
        throw new UnsupportedOperationException();
    }


    @Override
    public void checkDifficultyTransitions(final StoredBlock storedPrev, final Block nextBlock,
                                           final BlockStore blockStore) throws VerificationException, BlockStoreException {
        throw new UnsupportedOperationException();

    }
}
