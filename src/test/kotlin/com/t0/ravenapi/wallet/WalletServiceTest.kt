package com.t0.ravenapi.wallet

import org.assertj.core.api.Assertions.assertThat
import org.bitcoinj.core.ECKey
import org.bitcoinj.core.Utils
import org.junit.jupiter.api.Test

import java.math.BigInteger

internal class WalletServiceTest {

    private val walletService = WalletServiceImpl()

    @Test
    fun signTransaction() {

        // Setup - data
        /**
         * A known, valid private key that can construct a valid [ECKey]
         */
        val privateKey = "34341926953372365241540022703540154250340979601796301195118209085770006269035"

        /**
         * A known, valid transaction hex that can be signed
         */
        val transactionHex =
            "02000000013102bc6f3e2c2d972efae33499b8f924cefeabaff699a6f21c46a4bed7f5fe89000000001976a91420d0a0a5ce79f0941a30e61f54b6bcb5a2294db888acffffffff010008af2f000000001976a91407d37f040423f65f3c921bff93bdda429c46cea688ac00000000"

        val ecKey = ECKey.fromPrivate(BigInteger(privateKey))

        // Execute
        val signedTransaction = walletService.signTransaction(ecKey, transactionHex)

        // Verify
        assertThat(signedTransaction.signedTx)
            .isEqualTo(Utils.HEX.decode("02000000013102bc6f3e2c2d972efae33499b8f924cefeabaff699a6f21c46a4bed7f5fe89000000006b483045022100bd464bfafa7b6004eedbcec4ecf24c02efad09a7ecceb821a7559fa01a405a6c0220441310e7370f47340e2214f777cd97abf1de4317e8d9679d1657baf0cfd135ec0121034bc268481ff355da618e2fe80a4e6da4475767cfbb301179908bed261a6a84e7ffffffff010008af2f000000001976a91407d37f040423f65f3c921bff93bdda429c46cea688ac00000000"))
    }

    /**
     * Can also be verified via https://iancoleman.io/bip39/
     */
    @Test
    fun `restore HD wallet`() {
        val wallet = walletService.restore(tZeroUserId = 1001, index = 10)
        println(wallet)

        assertThat(wallet.address).isEqualTo("miDvPSgag1FgBAcTXqGTYCrepNmKG9Wssc")
        assertThat(wallet.key.pathAsString).isEqualTo("M/44H/1H/0H/1001/10")
    }
}
