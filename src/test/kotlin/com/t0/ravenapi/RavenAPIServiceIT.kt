package com.t0.ravenapi

import com.t0.ravenapi.wallet.CreateTransactionRequest
import com.t0.ravenapi.wallet.RavenAPIClient
import com.t0.ravenapi.wallet.SendTransactionRequest
import com.t0.ravenapi.wallet.TransactionDetailsForAddressesRequest
import com.t0.ravenapi.wallet.UserAPIRequest
import com.t0.ravenapi.wallet.WalletService
import io.medici.kresult.asVal
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * High-level Workflow:
 * 1. provide a "hot" deposit address from a HD Wallet (uses tZERO API)
 * 2. using a funded RVN testnet account, transfer crypto to that deposit address (does not use a tZERO API)
 * 3. transfer crypto from "hot" to cold (uses tZERO API)
 */
@SpringBootTest
class RavenAPIServiceIT {

    /**
     * Bog-standard RVN testnet account. Does not need funding, nor do we need to know its keys.
     */
    private val TZERO_COLD_WALLET_ADDRESS = "mgELPQaS2oExWq5gmHi3Q4zbA5KBDELfqr"

    private val hotWallet = HotWalletTestContainer(
        privateKey = "cRZwi1ynqVZp21FQzEwGqA5HwGhmcdrHgCGqntbQGQnJEBV3MWkN",
        rvnAddress = "miDvPSgag1FgBAcTXqGTYCrepNmKG9Wssc",
        tZeroUserId = 1001,
        index = 10
    )

    @Autowired
    lateinit var apiClient: RavenAPIClient

    @Autowired
    lateinit var walletService: WalletService


    @Test
    @Order(1)
    fun `request deposit address`() {

        val wallet = walletService.restore(tZeroUserId = hotWallet.tZeroUserId, index = hotWallet.index)

        assertThat(wallet.address).isEqualTo(hotWallet.rvnAddress)
        assertThat(wallet.key.pathAsString).isEqualTo("M/44H/1H/0H/1001/10")
    }


    /**
     * 1. Use https://mangofarmassets.com/wallet to fund any testnet wallet
     * 2. Again using https://mangofarmassets.com/wallet, send crypto to [HotWalletTestContainer.rvnAddress]
     * 3. Wait for the transaction to complete, checking
     * https://rvnt.cryptoscope.io/address/?address=miDvPSgag1FgBAcTXqGTYCrepNmKG9Wssc
     */
    @Test
    @Order(2)
    fun `customer deposits crypto`() {
        assertThat(true).isTrue
    }

    @Order(3)
    @Test
    fun `transaction from hot to cold`() {

        apiClient.tryCreateToken(
            UserAPIRequest(
                email = UUID.randomUUID().toString() + "@gmail.com",
                p = "Passw0rd",
                uuid = UUID.randomUUID()
            )
        ).verifySuccess { api ->

            val jwt = api.token

            apiClient.tryCreateTransaction(
                authHeader = jwt,
                CreateTransactionRequest(
                    fromAddresses = listOf(hotWallet.rvnAddress),
                    recipient = TZERO_COLD_WALLET_ADDRESS,
                    sendMax = false,
                    deductFee = true,
                    amount = BigDecimal("3.00"),
                    changeAddress = hotWallet.rvnAddress
                )
            ).verifySuccess { transResponse ->

                println("Create Transaction Response: $transResponse")

                val signedHex = walletService.sign(transResponse.unsignedHex).asVal()

                // commented out code calls the raven node rpc API to sign

//                apiClient.trySignTransaction(
//                    authHeader = jwt,
//                    SignTransactionRequest(
//                        keys = arrayOf(hotWallet.privateKey),
//                        transaction = transResponse.unsignedHex
//                    )
//                ).verifySuccess { asVal ->
//                    val signedHex = asVal.hex

                apiClient.trySendTransaction(
                    authHeader = jwt,
                    SendTransactionRequest(
                        transaction = signedHex
                    )
                ).verifySuccess { sendTransactionResponse ->
                    println("Transaction Id  $sendTransactionResponse")

                    TimeUnit.SECONDS.sleep(10) // let nodes sync before retrieving..

                    apiClient.tryGetTransactionDetailsForAddresses(
                        authHeader = jwt,
                        TransactionDetailsForAddressesRequest(
                            addresses = listOf(TZERO_COLD_WALLET_ADDRESS),
                            transactionIds = listOf(sendTransactionResponse.transactionId)
                        ).also {
                            println("Manually Check ME!! $it")
                        }
                    ).verifySuccess {
                        println("TransactionDetailsForAddressesRequest: $it")
                    }
                }
            }
        }
    }


    /**
     * Simple test that calls a "newly built TEMP" raven-service-api
     * https://gitlab.com/tzero-git/tzero-wallet/services/raven-api-service/-/pipelines/390289447
     */
    @Test
    fun `verify pipeline`() {

        apiClient.tryCreateToken(
            UserAPIRequest(
                email = UUID.randomUUID().toString() + "@gmail.com",
                p = "Passw0rd",
                uuid = UUID.randomUUID()
            )
        ).verifySuccess { api ->
            apiClient.tryGetBlockCountDamo(api.token).verifySuccess {
                assertThat(it.blockCount > 0).isTrue()
            }
        }
    }
}


/**
 * Maps from https://iancoleman.io/bip39/ , and "Derived Addresses" section
 */
data class HotWalletTestContainer(

    val privateKey: String =
        "cQ8HncK9v4wP3brr9nYgNk6V3AyyqbxfoMWcK5U5mDyDpvU4dFkA",

    val rvnAddress: String = "miWTqtRpisPDCt71m51zdLRKV9LRfpViCz",

    val tZeroUserId: Int,

    val index: Int
)

