package com.t0.ravenapi

import com.t0.ravenapi.wallet.TZeroException
import com.t0.ravenapi.wallet.TZeroResponse
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.fail

fun <T> TZeroResponse<T>.verifySuccess(block: (T) -> Unit): T {
    return this.e?.let { fail("Exception thrown from client", it) } ?: this.response?.let {
        Assertions.assertThat(it.statusCode.is2xxSuccessful).isEqualTo(true)
        it.body?.let { body ->
            block(body)
            body
        } ?: fail { "response is null" }
    } ?: fail {
        "ResponseEntity is null"
    }
}

fun <T> TZeroResponse<T>.verifyFailure(block: (TZeroException) -> Unit) {
    this.e?.let { block(it) } ?: this.response?.let {
        fail { "Expected to fail. Result: $it" }
    } ?: fail { "Expected to fail." }
}